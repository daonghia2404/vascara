import React from "react";
import "rsuite/dist/styles/rsuite-default.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "./app.scss";
import "./index.scss";

import { Layout } from "layout/layout";
import { Homepage } from "routes/home/home";
import { Category } from "routes/category/category";
import { ProductDetail } from "routes/product-detail/product-detail";
import { Cart } from "routes/cart/cart";
import { SignUp } from "routes/sign-up/sign-up";
import { SignIn } from "routes/sign-in/sign-in";

import { BrowserRouter as Router, Route } from "react-router-dom";

const App = () => {
  return (
    <Router>
      <div className="app">
        <Layout>
          <Route path="/" exact component={Homepage} />
          <Route path="/category/:category" exact component={Category} />
          <Route path="/product-detail" exact component={ProductDetail} />
          <Route path="/cart" exact component={Cart} />
          <Route path="/sign-up" exact component={SignUp} />
          <Route path="/sign-in" exact component={SignIn} />
        </Layout>
      </div>
    </Router>
  );
};

export default App;
