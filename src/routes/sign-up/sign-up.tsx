import React from "react";
import { Link } from "react-router-dom";
import "./sign-up.scss";

export const SignUp = () => {
  return (
    <div className="sign-up">
      <h2 className="title-block" style={{ color: "black" }}>
        ĐĂNG KÝ THÀNH VIÊN
      </h2>
      <form action="#">
        <div className="form-group">
          <div className="label">Email của bạn</div>
          <input type="text" placeholder="Email" />
        </div>
        <div className="form-group">
          <div className="label">Điện thoại</div>
          <input type="text" placeholder="Số Điện thoại" />
        </div>
        <div className="form-group">
          <div className="label">Mật khẩu của bạn</div>
          <input type="password" placeholder="Mật khẩu" />
        </div>
        <div className="form-group">
          <div className="label">Họ và tên</div>
          <input type="text" placeholder="Vui lòng nhập tiếng Việt có dấu" />
        </div>
        <p>
          Bằng cách bấm nút "Đăng ký" bên dưới, bạn đã xác nhận đồng ý với{" "}
          <Link to="/">Điều khoản sử dụng thông tin</Link> của vascara.com
        </p>
        <div className="form-group">
          <div className="custom-button">Đăng ký</div>
        </div>
        <div className="quick-link d-flex align-items-center">
          <Link to="/sign-in">Đăng nhập</Link>
          <span>|</span>
          <Link to="/">Quên mật khẩu</Link>
        </div>
      </form>
    </div>
  );
};
