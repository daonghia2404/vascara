// @ts-ignore
// @ts-nocheck

import React, { useState, useEffect } from "react";
import "./cart.scss";
import { DeleteIcon } from "components/icons/icons";
import { Link } from "react-router-dom";
import { getLocalStorage, setLocalStorage } from "utils/local-storage";

export const Cart = () => {
  const [allCart, setAllCart] = useState([]);
  let [price, setPrice] = useState(0);
  let [sale, setSale] = useState(0);
  const [totalQuanlity, setTotalQuanlity] = useState(0);

  useEffect(() => {
    getCart();
  }, []);

  const getCart = () => {
    let cart = getLocalStorage("cart");
    if (!cart) return;
    setAllCart(cart);
    getPrice(cart);
    totalAmount(cart);
  };

  const getPrice = (carts) => {
    let price = 0;
    let sale = 0;

    carts.map((item: any) => {
      if (item.sale === 0) {
        price += item.price * item.total;
      } else {
        price += item.sale * item.total;
        sale += item.sale * item.total - item.price * item.total;
      }
    });
    setPrice(price);
    setSale(sale);
  };

  const totalAmount = (carts) => {
    let total = 0;
    carts.map((item: any) => (total += item.total));
    setTotalQuanlity(total);
  };

  const deleteCart = (id: number) => {
    const newCart = allCart.filter((product, index) => index !== id);
    setLocalStorage("cart", newCart);
    getCart();
  };

  const amountManagement = (type: string, index: any) => {
    if (type === "minus") allCart[index].total > 1 && allCart[index].total--;
    else if (type === "plus") allCart[index].total++;
    setLocalStorage("cart", allCart);
    getCart();
  };

  return (
    <div className="cart">
      <div className="cart-wrap section-wrap">
        <h2 className="title-block">Giỏ hàng của bạn</h2>
        {allCart.length === 0 ? (
          <p className="notify-cart d-flex align-items-center justify-content-center flex-column">
            Hiện tại không có sản phẩm nào trong giỏ hàng của bạn{" "}
            <Link to="/" className="custom-button">
              Tiếp tục mua sắm
            </Link>
          </p>
        ) : (
          <div>
            <table>
              <thead>
                <tr>
                  <td>Ảnh sản phẩm</td>
                  <td>Tên sản phẩm</td>
                  <td>Đơn giá</td>
                  <td>Số lượng</td>
                  <td>Thành tiền</td>
                  <td>Xóa</td>
                </tr>
              </thead>
              <tbody>
                {allCart.map((item: any, index: number) => (
                  <tr key={index}>
                    <td>
                      <img src={item.image} alt="" />
                    </td>
                    <td>{item.title}</td>
                    <td>
                      {item.price
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                      ₫
                    </td>
                    <td>
                      <div className="amount">
                        <span
                          className="minus"
                          onClick={() => amountManagement("minus", index)}
                        >
                          -
                        </span>
                        <span className="value">{item.total}</span>
                        <span
                          className="plus"
                          onClick={() => amountManagement("plus", index)}
                        >
                          +
                        </span>
                      </div>
                    </td>
                    <td>
                      {(item.price * item.total)
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                      ₫
                    </td>
                    <td>
                      <DeleteIcon onClick={() => deleteCart(index)} />
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>

            <div className="order">
              <table className="order-table col-lg-6 col-12">
                <tbody>
                  <tr>
                    <td>Số lượng</td>
                    <td>
                      <strong>{totalQuanlity}</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>Giá trị hàng hóa</td>
                    <td>
                      {price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}₫
                    </td>
                  </tr>
                  <tr>
                    <td>Phí vận chuyển</td>
                    <td>
                      <em>
                        <strong>Chưa có</strong>
                      </em>
                    </td>
                  </tr>
                  <tr>
                    <td>Giảm tiền</td>
                    <td className="primary-color">
                      {sale.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}₫
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Thành tiền <span>(đã bao gồm VAT)</span>
                    </td>
                    <td>
                      {(price - sale)
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                      ₫
                    </td>
                  </tr>
                </tbody>
              </table>
              <div className="custom-button order-btn">
                Tiến hành thanh toán
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
