import React, { useEffect } from "react";
import "./category.scss";
import { CategorySection } from "components/category-section/category-section";
import { Link } from "react-router-dom";
import mockupDataNewest from "data/mockupDataNewest";
import mockupDataDiscount from "data/mockupDataDiscount";
import mockupDataBestSeller from "data/mockupDataBestSeller";
import mockupDataHot from "data/mockupDataHot";

export const Category = ({ match }: any) => {
  const currentCategory = match.params.category;
  const banner = [
    require("assets/images/category-image-1.png"),
    require("assets/images/category-image-2.png"),
    require("assets/images/category-image-3.png"),
    require("assets/images/category-image-4.png"),
  ];

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <div className="category">
      <div className="category-banner">
        {currentCategory === "san-pham-moi-nhat" ? (
          <img src={banner[0]} alt="" />
        ) : currentCategory === "san-pham-khuyen-mai" ? (
          <img src={banner[1]} alt="" />
        ) : currentCategory === "san-pham-ban-chay" ? (
          <img src={banner[2]} alt="" />
        ) : currentCategory === "san-pham-noi-bat" ? (
          <img src={banner[3]} alt="" />
        ) : (
          ""
        )}
      </div>

      {currentCategory === "san-pham-moi-nhat" ? (
        <CategorySection
          title="SẢN PHẨM MỚI NHẤT"
          link="/"
          dataProduct={mockupDataNewest}
        />
      ) : currentCategory === "san-pham-khuyen-mai" ? (
        <CategorySection
          title="SẢN PHẨM KHUYẾN MÃI"
          link="/"
          dataProduct={mockupDataDiscount}
        />
      ) : currentCategory === "san-pham-ban-chay" ? (
        <CategorySection
          title="SẢN PHẨM BÁN CHẠY"
          link="/"
          dataProduct={mockupDataBestSeller}
        />
      ) : currentCategory === "san-pham-noi-bat" ? (
        <CategorySection
          title="SẢN PHẨM NỔI BẬT"
          link="/"
          dataProduct={mockupDataHot}
        />
      ) : (
        ""
      )}

      <div className="category-footer d-flex justify-content-center">
        <Link className="see-more-category" to="/">
          Xem thêm các sản phẩm khác
        </Link>
      </div>
    </div>
  );
};
