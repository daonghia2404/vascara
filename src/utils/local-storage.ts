// @ts-nocheck
// @ts-ignore

export const getLocalStorage = (key, data) => {
  return localStorage.getItem(key)
    ? JSON.parse(decodeURI(localStorage.getItem(key)))
    : [];
};

export const setLocalStorage = (key, data) => {
  data = encodeURI(JSON.stringify(data));
  localStorage.setItem(key, data);
};
