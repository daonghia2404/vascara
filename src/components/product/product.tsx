// @ts-nocheck
// @ts-ignore

import React from "react";
import "./product.scss";
import classNames from "classnames";
import { Link } from "react-router-dom";
import { Notification } from "rsuite";
import { getLocalStorage, setLocalStorage } from "utils/local-storage";

type Product = {
  image: string;
  price: number;
  sale: number;
  title: string;
  status: {
    type: string;
    content: string;
  };
};

type Props = {
  dataProduct: any;
  index: number;
  image: string;
  price: number;
  sale: number;
  title: string;
  status: {
    type: string;
    content: string;
  };
  className?: string;
};

const openNotification = () => {
  Notification["success"]({
    title: "Thông báo",
    placement: "bottomEnd",
    description: "Thêm vào giỏ hàng thành công",
  });
};

const addToCart = (product: Product) => {
  let cart = getLocalStorage("cart");
  cart.push(product);
  setLocalStorage("cart", cart);
  openNotification();
};

export const Product = ({
  dataProduct,
  index,
  image,
  price,
  sale,
  title,
  status,
  className,
}: Props) => {
  return (
    <Link to="/product-detail" className={classNames("product", className)}>
      <div className="product-image">
        <img src={image} alt="" />
      </div>
      <div className="product-price d-flex">
        <p className="price">
          {price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")} ₫
        </p>
        {sale !== 0 && (
          <div className="price del">
            {sale.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")} ₫
          </div>
        )}
      </div>
      <h2 className="product-title">{title}</h2>
      {status.type !== "" && status.type === "new" ? (
        <div className="price-status">{status.content}</div>
      ) : (
        status.type !== "" && (
          <div className="price-status active">{status.content}</div>
        )
      )}
      <div
        className="add-to-cart custom-button"
        onClick={(e) => {
          e.preventDefault();
          addToCart(dataProduct[index]);
        }}
      >
        Thêm vào giỏ hàng
      </div>
    </Link>
  );
};
